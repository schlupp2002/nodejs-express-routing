const winston = require('winston');
const express = require('express');
const logger = require('./logger/logger');

const app = express();

const httpPort = 9000;

// a central request logger 
app.use((req, res, next)=>{
    logger.log('info', '%s %s', req.method, req.url);
    next(); 
});

// respond with static text when a GET is made to the root
app.get('/', (req, res) => {
    res.send('GET request to the homepage');
});

/**
 * Route methods 
 *
 * supported are: get, post, put, head, delete, options, trace, copy, lock, mkcol, move, purge, propfind,
 * proppatch, unlock, report, mkactivity, checkout, merge, m-search, notify, subscribe, 
 * unsubscribe, patch, search, and connect.
 */
app.post('/', (req, res) => {
    res.send('POST request to the homepage');
});

app.put('/', (req, res) => {
    res.send('PUT request to the homepage');
});

app.all('/all*', (req, res, next) => {
    res.send('The ALLspark was requested via: ' + req.method);
});

/**
 * Route paths
 *
 */
app.get('/ab?cd', (req,res) => {
    res.send('/ab?cd ---> the b is optional, matches the routes /acd AND /abcd');
});

app.get('/ab+cd', (req, res) => {
    res.send('/ab+cd ---> at least one b is required, matches the routes /abcd, /abbcd, /abbbb, ... ');
});

app.get(/.*fly$/, (req,res) => {
    res.send('/.*fly$/ ---> a string ending with fly is required');
});

/**
 * Route paramaters
 *
 */
app.get('/users/:userId/books/:bookId', (req, res) => {
    res.send('Route with params: ' + JSON.stringify(req.params));
});

app.get('/flights/:from-:to', (req, res) => {
    res.send('Flights from ' + req.params.from + ' to ' + req.params.to);
});

/**
 * Route handlers
 *
 */
const rh1 = (req, res, next) => {
    res.answer = 'Message from route handler one';
    next();
}; 

const rh2 = (req, res, next) => {
    res.answer = 'Message from route handler two';
    next();
};

const rh3 = (req, res, next) => {
    res.answer += 'Message from route handler three';
    res.send(res.answer);
    next();
};

app.get('/rh1', [rh1, rh2, rh3]);


// start the server
app.listen(httpPort, (err) => {
    if(err) {
        logger.log('error', 'An error occurred while trying to listen to port %d', httpPort);
    }
});
