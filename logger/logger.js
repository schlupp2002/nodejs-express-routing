const winston = require('winston');
const fs = require('fs');

// setting up the log directory and make sure it exists
const logDir = process.cwd() + '/log';

if(!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);  
};

// setting up the time format for logging
const dateTimeFormat = () => (new Date()).toISOString();

// setting up the levels and colors used within this project
const config = {
    levels: {
        error: 0,
        debug: 1,
        warn: 2,
        data: 3,
        info: 4,
        verbose: 5,
        silly: 6
    },
    colors: {
        error: 'red',
        debug: 'blue',
        warn: 'yellow',
        data: 'grey',
        info: 'green',
        verbose: 'cyan',
        silly: 'magenta'
    }
};

const logger = module.exports = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)({
            colorize: true,
        }),
        new (winston.transports.File)({
            filename: `${logDir}/main.log`,
            timestamp: dateTimeFormat,
        }),
        
    ],
    levels: config.levels,
    color: config.colors
});
